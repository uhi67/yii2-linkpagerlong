# LinkPagerLong

For yii2 applications.

**Version 1.1 (Released 2022-01-09)**

This class replaces LinkPager to display enhanced pager buttons for very long lists.

Displays the following paging buttons:

- First page icon
- Previous page icon
- First some page
- Three proportional intermediate pages between first and current (displayed as dots)
- Some pages around current page (current is diifferent colour)
- Three proportional intermediate pages between current and last (displayed as dots)
- Last some page
- Next page icon
- Last page icon

For best result, set maxButtonCount at least 10, but recommended is 15 or above.

### Install

- `composer require uhi67/yii2-linkpagerlong:~1.1`

### Example

Pager pattern if maxButtonCount is 15, current page is 18, last page is 50:

`[<<] [<] [1] [2] [.] [.] [.] [16] [17] [18] [19] [20] [.] [.] [.] [49] [50] [>] [>>]`

`[$beginPage=16, $endPage=20, $firstPages=2, $lastPages=2, $beginDots=3, $endDots=3];`

The dots represent proportional plots between central and edge ranges: 5, 9, 12 and 27, 34, 41

### Example 

Pager pattern if maxbuttons is 10, current page is 18, last page is 22:

`. . 16 17 18 19 20 21 22` 

(Dots represent 5, 10)

## Usage in views

```html
<?= GridView::widget([
  'pager' => [
        'class' => \uhi67\linkpagerlong\LinkPagerLong::class,
        'prevPageLabel' => '<i class="fa fa-step-backward"></i>',
        'nextPageLabel' => '<i class="fa fa-step-forward"></i>',
        'firstPageLabel' => '<i class="fa fa-fast-backward"></i>',
        'lastPageLabel' => '<i class="fa fa-fast-forward"></i>',
        'maxButtonCount' => 15,
        'messageSource' => 'app',
    ],
    ...
?>
```

Hints are shown on hovering on buttons. If hint translation is needed, please specify a message source in `messageSource` option.

## Settings

All inherited properties of LinkPager class, plus:

- maxButtonCount: maximum number of buttons including first, previous, next and last buttons. Default value is 13.
- messageSource: message source name for translations of button hints (titles) or empty if no translation is needed

# Change Log

## 1.1 (2022-01-09)

- button hints and messageSource option is added

