<?php
namespace uhi67\linkpagerlong;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\LinkPager;

/**
 *
 * @property array $pageRange
 */
class LinkPagerLong extends LinkPager {
    public $maxButtonCount = 13;
    /** @var string message source name for translations of button hints (titles) or empty if no translation is needed */
    public $messageSource;
    /**
     * ## Renders the page buttons.
     * Replaces LinkPager to display enhanced pager buttons for very long lists.
     *
     * ### Example
     * Pager pattern if maxButtonCount is 15, current page is 18, last page is 50:
     *
     * `1 2 . . . 16 17 18 19 20 . . . 49 50`
     *
     * `[$beginPage=16, $endPage=20, $firstPages=2, $lastPages=2, $beginDots=3, $endDots=3];`
     *
     * The dots represent proportional plots between central and edge ranges: 5, 9, 12 and 27, 34, 41
     *
     * ### Example Pager pattern if maxbuttons is 10, current page is 18, last page is 22:
     *
     * `. . 16 17 18 19 20 21 22 (Dots represent 5, 10)`
     *
	 *	<?= GridView::widget([
	 *       'pager' => [
	 *			'class' => LinkPagerLong::className(),
	 *			'prevPageLabel' => '<span class="glyphicon glyphicon-backward"></span>',
	 *			'nextPageLabel' => '<span class="glyphicon glyphicon-forward"></span>',
	 *			'firstPageLabel' => '<span class="glyphicon glyphicon-fast-backward"></span>',
	 *			'lastPageLabel' => '<span class="glyphicon glyphicon-fast-forward"></span>',
	 *		],
	 *		...
	 *	?>
     *
     * @return string the rendering result
     */
    protected function renderPageButtons()
    {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            return '';
        }

        $buttons = [];
        $currentPage = $this->pagination->getPage();

        // first page
        $firstPageLabel = $this->firstPageLabel === true ? '1' : $this->firstPageLabel;
        if ($firstPageLabel !== false) {
            $buttons[] = $this->renderPageButton($firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false, 'First page');
        }

        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $currentPage - 1) < 0) {
                $page = 0;
            }
            $buttons[] = $this->renderPageButton($this->prevPageLabel, $page, $this->prevPageCssClass, $currentPage <= 0, false, 'Previous page');
        }

        // --- The difference begins here.
	        list($beginPage, $endPage, $firstPages, $lastPages, $beginDots, $endDots) = $this->getPageRange();
	        // first pages
	        for ($i = 0; $i < $firstPages; ++$i) {
	            $buttons[] = $this->renderPageButton($i + 1, $i, null, false, false);
	        }
	        // dot range between first pages and internal pages
	        for ($i = 0; $i < $beginDots; ++$i) {
	        	$di = $this->prop($firstPages, $beginPage, $beginDots, $i);
	            $buttons[] = $this->renderPageButton('.', $di, null, false, false);
	        }
	        // internal pages
	        for ($i = $beginPage; $i <= $endPage; ++$i) {
	            $buttons[] = $this->renderPageButton($i + 1, $i, null, false, $i == $currentPage, $i == $currentPage ? 'Current page' : '');
	        }
	        // dot range between internal pages and last pages
	        for ($i = 0; $i < $endDots; ++$i) {
	        	$di = $this->prop($endPage, $pageCount - $lastPages, $endDots, $i);
	            $buttons[] = $this->renderPageButton('.', $di, null, false, false, ['Page {page}', 'page'=>$di]);
	        }
	        // Last pages
	        for ($i = 0; $i < $lastPages; ++$i) {
	            $buttons[] = $this->renderPageButton($pageCount - $lastPages + $i + 1, $pageCount - $lastPages + $i, null, false, false);
	        }
		// --- The difference ends here

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $currentPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }
            $buttons[] = $this->renderPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false, 'Next page');
        }

        // last page
        $lastPageLabel = $this->lastPageLabel === true ? $pageCount : $this->lastPageLabel;
        if ($lastPageLabel !== false) {
            $buttons[] = $this->renderPageButton($lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false, 'Last page');
        }

        return Html::tag('ul', implode("\n", $buttons), $this->options);
    }

    /**
     * @return array -- beginPage, endPage, firstPages, lastPages, beginDots, endDots
     * 0-based page numbers
     */
    protected function getPageRange()
    {
    	$dcx = [0=>5, 7=>3, 9=>4, 14=>5, 20=>6];
    	$ecx = [0=>11, 15=>7, 20=>8];

        $currentPage = $this->pagination->getPage();
        $pageCount = $this->pagination->getPageCount();

        /** @var int $dc -- number of dots before and after internal */
        foreach($dcx as $c=>$x) {
        	if($this->maxButtonCount >= $c) $dc = (int)floor($this->maxButtonCount/$x);
        	else break;
        }
        $beginDots = $endDots = $dc;

        /** @var int $ec -- number of edge (begin and end) pages */
        foreach($ecx as $c=>$x) {
        	if($this->maxButtonCount >= $c) $ec = (int)floor($this->maxButtonCount/$x);
        	else break;
        }
        $firstPages = $lastPages = $ec;

        /** @var int $ic -- number of internal pages */
        $ic = $this->maxButtonCount - 2*($dc + $ec);

        $beginPage = max(0, $currentPage - (int) ($ic / 2));
        if (($endPage = $beginPage + $ic - 1) >= $pageCount) {
            $endPage = $pageCount - 1;
            $beginPage = max(0, $endPage - $ic + 1);
        }

        // corrections near edges
        if($beginPage<$dc+$ec) { $beginPage = 0; $firstPages = 0; $beginDots = 0; }
        if($endPage>$pageCount-$dc-$ec) { $endPage = $pageCount-1; $lastPages = 0; $endDots = 0; }

        return [$beginPage, $endPage, $firstPages, $lastPages, $beginDots, $endDots];
    }

	/**
	 * Computes a proportional split point between e1 and e2, returns $index of $num dividers
	 *
	 * @param int $e1 -- range start
	 * @param int $e2 -- range end
	 * @param int $num -- number of dividers
	 * @param int $i -- index of divider to return
	 * @return int -- the i-th divider in the range
	 */
    private function prop($e1, $e2, $num, $i) {
    	return $e1 + (int)(($e2-$e1)/($num+1)*($i+1));
    }

    /**
     * Renders a page button.
     * You may override this method to customize the generation of page buttons.
     * @param string $label the text label for the button
     * @param int $page the page number
     * @param string $class the CSS class for the page button.
     * @param bool $disabled whether this page button is disabled
     * @param bool $active whether this page button is active
     * @param string $hint title attribute for link wrap tag if not empty; optionally will be translated if messageSource is defined
     * @return string the rendering result
     */
    protected function renderPageButton($label, $page, $class, $disabled, $active, $hint='')
    {
        $options = $this->linkContainerOptions;
        $linkWrapTag = ArrayHelper::remove($options, 'tag', 'li');
        Html::addCssClass($options, empty($class) ? $this->pageCssClass : $class);

        if ($active) {
            Html::addCssClass($options, $this->activePageCssClass);
        }
        if ($disabled) {
            Html::addCssClass($options, $this->disabledPageCssClass);
            $disabledItemOptions = $this->disabledListItemSubTagOptions;
            $tag = ArrayHelper::remove($disabledItemOptions, 'tag', 'span');
            return Html::tag($linkWrapTag, Html::tag($tag, $label, $disabledItemOptions), $options);
        }
        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;

        if($hint) {
            if(!$this->messageSource && is_array($hint)) $options['title'] = $hint[0];
            else if(!$this->messageSource) $options['title'] = $hint;
            else if(is_array($hint)) $options['title'] = \Yii::t($this->messageSource, $hint[0], $hint);
            else $options['title'] = \Yii::t($this->messageSource, $hint);
        }
        return Html::tag($linkWrapTag, Html::a($label, $this->pagination->createUrl($page), $linkOptions), $options);
    }

}
